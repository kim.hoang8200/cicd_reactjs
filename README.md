# You require 3 Ubuntu servers for project execution.

## Performed code push to the staging branch.
![Alt text](image/image.png)
# The CI/CD process is in progress
![Alt text](image/image2.png)
![Alt text](image/image1.png)
## It utilizes the Runner on the Ubuntu host.
![Alt text](image/image5.png) ![Alt text](image/image6.png)
![Alt text](image/image7.png)

## Result.
![Alt text](image/image4.png)
### It is deployed on the Ubuntu server
![Alt text](image/image3.png)
